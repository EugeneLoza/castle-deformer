{ Main view, where most of the application logic takes place.

  Feel free to use this code as a starting point for your own projects.
  This template code is in public domain, unlike most other CGE code which
  is covered by BSD or LGPL (see https://castle-engine.io/license). }
unit GameViewMain;

interface

uses Classes, Generics.Collections,
  CastleVectors, CastleComponentSerialize, CastleGLImages,
  CastleUIControls, CastleControls, CastleKeysMouse;

type
  { Main view, where most of the application logic takes place. }
  TViewMain = class(TCastleView)
  strict private
    Kad, Kab: TVector2;
    Length30, Length10: Single;
    procedure TransformCache;
  strict private
    Pts: array [0..3] of TVector2;
    { Transform Destination coordinates into source image coordinates }
    function Transform(const Destination: TVector2): TVector2;
  strict private
    CurrentFrame: Integer;
    Frames: specialize TObjectList<TDrawableImage>;
  published
    { Components designed using CGE editor.
      These fields will be automatically initialized at Start. }
    LabelFps: TCastleLabel;
    ImageControl1, ImageControl2: TCastleImageControl;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Stop; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  ViewMain: TViewMain;

implementation

uses
  SysUtils,
  CastleLog, CastleImages;

{ TViewMain ----------------------------------------------------------------- }

{ Calculates intersection point between 2 lines defined by points (A, B) and (D, C) }
function Intersect(const A, B, D, C: TVector2): TVector2; inline;
var
  K: Single;
begin
  K := ((D.Y - A.Y) * (B.X - A.X) - (D.X - A.X) * (B.Y - A.Y)) /
       ((C.X - D.X) * (B.Y - A.Y) - (C.Y - D.Y) * (B.X - A.X));
  Result.X := D.X + K * (C.X - D.X);
  Result.Y := D.Y + K * (C.Y - D.Y);
end;

procedure TViewMain.TransformCache;
begin
  Kad := Intersect(Pts[0], Pts[1], Pts[3], Pts[2]);
  Kab := Intersect(Pts[1], Pts[2], Pts[0], Pts[3]);
  Length30 := (Pts[3] - Pts[0]).Length;
  Length10 := (Pts[1] - Pts[0]).Length;
end;

function TViewMain.Transform(const Destination: TVector2): TVector2;
var
  Ax, Ay: TVector2;

  function Sign(const Value: Single): Integer; inline;
  begin
    if Value > 0 then
      Exit(1)
    else
      Exit(-1);
  end;

begin
  Ax := Intersect(Pts[0], Pts[3], Kad, Destination);
  Ay := Intersect(Pts[0], Pts[1], Kab, Destination);

  // Warning: sign is a temporary solution, as it requires Pts[0] to be the bottom-left point of the image, which may not be true
  Result.X := Sign(Ax.X - Pts[0].X) * (Ax - Pts[0]).Length / Length30;
  Result.Y := Sign(Ay.Y - Pts[0].Y) * (Ay - Pts[0]).Length / Length10;
end;

constructor TViewMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gameviewmain.castle-user-interface';
end;

procedure TViewMain.Start;
var
  Src: TRGBAlphaImage;

  function TransformedImage(const A, B, C, D: TVector2): TRGBAlphaImage;
  var
    IX, IY: Integer;
    DstVec, SrcVec: TVector2;
    JX, JY: Integer;
  begin
    Pts[0] := A;
    Pts[1] := B;
    Pts[2] := C;
    Pts[3] := D;
    TransformCache;

    Result := TRGBAlphaImage.Create(Src.Width * 2, Src.Height * 2); // todo
    for IX := 0 to Pred(Result.Width) do
      for IY := 0 to Pred(Result.Height) do
      begin
        DstVec := Vector2(Single(IX) / Single(Result.Width) * 2, Single(IY) / Single(Result.Height) * 2);
        SrcVec := Transform(DstVec);
        JX := Round(Src.Width * SrcVec.X);
        JY := Round(Src.Height * SrcVec.Y);
        if (JX >= 0) and (JX < Src.Width) and (JY >= 0) and (JY < Src.Height) then
          Result.PixelPtr(IX, IY)^ := Src.PixelPtr(JX, JY)^
        else
          Result.PixelPtr(IX, IY)^ := Vector4Byte(0, 0, 0, 0);
      end;

  end;

begin
  inherited;
  Src := LoadImage('castle-data:/Smiley_green_alien_sword.png', [TRGBAlphaImage]) as TRGBAlphaImage;
  ImageControl1.Image := Src; // and owns it

  Frames := specialize TObjectList<TDrawableImage>.Create;

  //move
  Frames.Add(TDrawableImage.Create(TransformedImage(
      Vector2(0, 0),
      Vector2(0, 1),
      Vector2(1.05, 1.05),
      Vector2(1, 0)
    ), true, true));
  Frames.Add(TDrawableImage.Create(TransformedImage(
      Vector2(0, 0),
      Vector2(0.05, 1),
      Vector2(1.1, 1.05),
      Vector2(1, 0.05)
    ), true, true));
  Frames.Add(TDrawableImage.Create(TransformedImage(
      Vector2(0, 0),
      Vector2(0.05, 1.05),
      Vector2(1.05, 1.1),
      Vector2(0.95, 0.1)
    ), true, true));
  Frames.Add(TDrawableImage.Create(TransformedImage(
      Vector2(0, 0.05),
      Vector2(0.05, 1.05),
      Vector2(1.05, 1.1),
      Vector2(1.05, 0)
    ), true, true));
  Frames.Add(TDrawableImage.Create(TransformedImage(
      Vector2(0, 0.1),
      Vector2(0, 0.95),
      Vector2(0.95, 0.95),
      Vector2(1, 0)
    ), true, true));
end;

procedure TViewMain.Stop;
begin
  FreeAndNil(Frames);
  inherited Stop;
end;

procedure TViewMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  { This virtual method is executed every frame (many times per second). }
  Assert(LabelFps <> nil, 'If you remove LabelFps from the design, remember to remove also the assignment "LabelFps.Caption := ..." from code');
  LabelFps.Caption := 'FPS: ' + Container.Fps.ToString;

  Imagecontrol2.DrawableImage := Frames[CurrentFrame];
  ImageControl2.OwnsDrawableImage := false;
  Inc(CurrentFrame);
  if CurrentFrame >= Frames.Count then
    CurrentFrame := 0;
  Sleep(80);
end;

function TViewMain.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys

  { This virtual method is executed when user presses
    a key, a mouse button, or touches a touch-screen.

    Note that each UI control has also events like OnPress and OnClick.
    These events can be used to handle the "press", if it should do something
    specific when used in that UI control.
    The TViewMain.Press method should be used to handle keys
    not handled in children controls.
  }

  // Use this to handle keys:
  {
  if Event.IsKey(keyXxx) then
  begin
    // DoSomething;
    Exit(true); // key was handled
  end;
  }
end;

end.
